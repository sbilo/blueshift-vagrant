Vagrant
=======

This repository contains code to setup and run a local puppet node using vagrant and virtualbox it will install the puppet modules available at [https://gitlab.com/sbilo/projects](https://gitlab.com/sbilo/projects)

Installation
------------
Download and install virtualbox from [https://www.virtualbox.org/wiki/Downloads](https://www.virtualbox.org/wiki/Downloads)

Download and install vagrant from [https://www.vagrantup.com/downloads.html](https://www.vagrantup.com/downloads.html)

    git clone https://gitlab.com/sbilo/blueshift-vagrant.git
    
    cd blueshift-vagrant
    
    vagrant up